const express = require('express')
const config = require('config')
const mongoose = require('mongoose')
const app = express()

const stations = require('./routes/stations')
const cors = require('cors')

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));


app.use('/api/stations', stations)
const PORT = config.get('port') || 5000

async function start() {
    try {
        await mongoose.connect(config.get('mongoURI'), {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        })

        app.listen(PORT, ()=> console.log(`App has been started on port ${PORT} ...`))
    } catch (e) {
        console.log('Server error', e.message)
        process.exit(1)
    }
}

start()

