import React, {useEffect, useState} from 'react'
import {Link, useHistory} from 'react-router-dom'
import useAxios from "../hooks/useAxios"
import {NotificationContainer, NotificationManager} from 'react-notifications'
import './LinesList.css'
import 'react-notifications/lib/notifications.css'
import moment from 'moment'
import {Button, FormControl, InputGroup, Modal, Spinner, Tab, Tabs, Form} from "react-bootstrap"
import {linesInfo} from "../components/LinesInfo";
import axios from "axios";
import PersonPinIcon from "@material-ui/icons/PersonPin";


const RegisterTicketValidation = () => {
    const [stations, setStations] = useState([])
    const [spinner, setSpinner] = useState(true)
    const [stationsVisible, setStationsVisible] = useState(false)
    const [lineStations, setLineStations] = useState()
    const [confirmSubmit, setConfirmSubmit] = useState(false)
    const [stationSubmission, setStationSubmission] = useState(null)
    const history = useHistory()
    const [ip, setIP] = useState('')
    const [ips, setIPs] = useState([])
    const [spamError, setSpamError] = useState(false)
    const [key, setKey] = useState('defaultStations');
    // const [person, setPerson] = useState([])
    const [comment, setComment] = useState()
    const [conductorSelected, setConductorSelected] = useState(false)
    const [policeSelected, setPoliceSelected] = useState(false)
    const [settings, setSettings] = useState(null)
    const [validationTimeList, setValidationTimeList] = useState([])
    const metroLines = ['1', '2', '5', '6']
    const tramLines = ['3','4','7','8','9','19','25','39','44','51','55','62','81','82','92','93','97']
    const busLines = ['12', '13', '14',
        '17', '20', '21', '27', '28', '29', '33', '34', '36', '37', '38', '41', '42', '43', '45', '46', '47', '48',
        '49', '50', '52', '53', '54', '56', '57', '58', '59', '60', '61', '63', '64', '65', '66', '69', '70', '71'
    ]
   //


    useEffect(() => {
        let isMounted = true;
        const fetchStations = async() => {
            const result = await useAxios.get('stations')
            if (isMounted && result.status === 200)
                setStations(result.data)
            else if(result.error)
                NotificationManager.error('Internal Server Error', 'Error')
            setSpinner(false)
        }

        fetchStations()
        getData()
        return () => { isMounted = false };
    }, []);


    useEffect(() => {
        let isMounted = true;
        const fetchTicketValidation = async() => {
            const getTicketValidationTime = await useAxios.get('stations/ticketValidation')
            console.log('response', getTicketValidationTime)
            if (isMounted && getTicketValidationTime.status === 200)
                setValidationTimeList(getTicketValidationTime.data ? getTicketValidationTime.data.sort(
                    (a, b) =>  new Date(b.time).getTime() - new Date(a.time).getTime()):null)
            else if(getTicketValidationTime.error)
                NotificationManager.error('Internal Server Error', 'Error')
            setSpinner(false)

        }

        fetchTicketValidation()
        return () => { isMounted = false };

    }, []);

    useEffect(() => {
        let isMounted = true;
        const fetchSettings = async() => {
            const result = await useAxios.get('stations/settings')
            if (isMounted && result.status === 200)
                setSettings(result.data)
            else if(result.error)
                NotificationManager.error('Internal Server Error', 'Error')
            setSpinner(false)
        }

        fetchSettings()
        return () => { isMounted = false };
    }, []);


    console.log('settings....', settings && settings.spamLength)

    const addTime = async () => {
        let person = []
        if(conductorSelected && policeSelected) {
            person.push('conductor', 'police')
        } else if(policeSelected) {
            person.push('police')
        }  else if(conductorSelected) {
            person.push('conductor')
        }

         await useAxios.post('stations/ticketValidation', {station: stationSubmission, time: moment().format(), lineId: lineStations, ip: ip, person: person, comment: comment }).then(res => {
                 if (res.status === 200) {
                     NotificationManager.success('Ticket validation time & location registered successfully', 'Success')
                     history.push('/')
                 } else {
                     NotificationManager.error('Something went wrong', 'Error')
                 }
             }
         )
    }

    const getData = async () => {
        const res = await axios.get('https://geolocation-db.com/json/')
        setIP(res.data.IPv4)
    }


    const showStations = (line) => {
        setStationsVisible(true)
        setLineStations(line)

    }

    const handleClick = (e) => {

       const dt1 = validationTimeList.filter(i => i.ip === ip).filter(i => i.lineId === lineStations).find(i => i.station === e.currentTarget.dataset.id) ? new Date(validationTimeList.filter(i => i.ip === ip).filter(i => i.lineId === lineStations).find(i => i.station === e.currentTarget.dataset.id).time) :null ;

        const dt2 = new Date(moment().format())
        const dt3 = validationTimeList.find(i => i.ip === ip) ? new Date(validationTimeList.find(i => i.ip === ip).time ): null;

        function diff_minutes(dt2, dt1)
        {

            var diff =(dt2.getTime() - dt1.getTime()) / 1000;
            diff /= 60;
            return Math.abs(Math.round(diff));

        }

        if(validationTimeList.filter(i => i.ip === ip).filter(i => i.lineId === lineStations).find(i => i.station === e.currentTarget.dataset.id)) {
            console.log('ip and line and station exist')
             // if (diff_minutes(dt1, dt2) >= settings.resubmitLength ){
            if (diff_minutes(dt1, dt2) >= settings.resubmitLength ){
                 setStationSubmission(e.currentTarget.dataset.id)
                 setConfirmSubmit(true)
             }
                else {
                 setSpamError(true)
             }

        }

        else if(!validationTimeList.find(i => i.ip === ip)) {
            console.log('ip doesnt exist')
            setStationSubmission(e.currentTarget.dataset.id)
            setConfirmSubmit(true)
        }

        else if (validationTimeList.find(i => i.ip === ip)) {
            console.log('ip and line or station exist')
            if (diff_minutes(dt3, dt2) >= settings.spamLength ){
                setStationSubmission(e.currentTarget.dataset.id)
                setConfirmSubmit(true)
            }
            else {
                setSpamError(true)
            }
        }

         else {
            setSpamError(true)
            console.log('something else')
        }

        // setStationSubmission(e.currentTarget.dataset.id)
        //      setConfirmSubmit(true)

    }

console.log('asdfghjk', stations && stations.find(i=> i.lineId === lineStations) &&
    stations.find(i=> i.lineId === lineStations).points.slice(-1)[0].name)

    const handleCloseStations = () => setStationsVisible(false)

    const handleCloseConfirm = () => {
        setConfirmSubmit(false)
        setStationSubmission(null)
    }


    return (
        <div className="LinesList">
            <Link to='/'><Button variant="outline-light" style={{padding: '3px 10px'}}>Back</Button></Link>

            {
                spinner ?  <Spinner animation="border" style={{margin: '40%', display: 'block'}} /> :
                    (
                        <div style={{display: 'flex', flexDirection: 'column', marginTop: '20px'}}>
                            <h3 className='TransportType'>Metro</h3>
                            <div style={{display: 'flex', marginTop: '20px', alignContent: 'space-around'}}>
                            { stations && stations.filter(i => metroLines.includes(i.lineId)).map(i =>
                            <button className="line-stations-button" onClick={() =>showStations(i.lineId)}
                                    style={{background: linesInfo.find(j=> j.line === i.lineId)?
                                            linesInfo.find(j=> j.line === i.lineId).background: null
                                    }}>{i.lineId}</button>
                            )}
                            </div>
                            <h3 className='TransportType'>Tram</h3>
                            <div style={{display: 'flex', marginTop: '20px', flexWrap: 'wrap', alignContent: 'space-around'}}>
                                { stations && stations.filter(i => tramLines.includes(i.lineId)).map(i =>
                                    <button className="line-stations-button" onClick={() =>showStations(i.lineId)}
                                            style={{background: linesInfo.find(j=> j.line === i.lineId)?
                                                    linesInfo.find(j=> j.line === i.lineId).background: null}}>{i.lineId}</button>
                                )}
                            </div>
                            <h3 className='TransportType'>Bus</h3>
                            <div style={{display: 'flex', marginTop: '20px', flexWrap: 'wrap', alignContent: 'space-around'}}>
                                { stations && stations.filter(i => busLines.includes(i.lineId)).map(i =>
                                    <button className="line-stations-button" onClick={() =>showStations(i.lineId)}
                                            style={{background: linesInfo.find(j=> j.line === i.lineId)?
                                                    linesInfo.find(j=> j.line === i.lineId).background: 'blue'}}>{i.lineId}</button>
                                )}
                            </div>
                            <Modal show={stationsVisible} onHide={handleCloseStations} >
                                <Modal.Header closeButton>
                                    <Modal.Title>Line {lineStations}</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Tabs
                                        id="controlled-tab-example"
                                        activeKey={key}
                                        onSelect={(k) => setKey(k)}
                                        className="mb-3"
                                    >
                                        <Tab eventKey="defaultStations" title="stations">
                                            <ul className="line-stations">
                                                { stations && stations.find(i=> i.lineId === lineStations) ?
                                                    stations.find(i=> i.lineId === lineStations).points.map(i =>
                                                    <li className="line1-stations-item"
                                                        onClick={handleClick.bind(this)}
                                                        data-id={i.name}
                                                        key={i.name}>
                                                        {i.name}
                                                    </li>
                                                ) : null}
                                            </ul>
                                        </Tab>
                                        <Tab eventKey="left" title={`> ${stations && stations.find(i=> i.lineId === lineStations) &&
                                        stations.find(i=> i.lineId === lineStations).points.slice(-1)[0].name}`}>
                                            <ul className="line-stations">
                                                {
                                                    stations && stations.find(i=> i.lineId === lineStations) ?
                                                        stations.find(i=> i.lineId === lineStations).points.map(i =>
                                                    <li className="line1-stations-item"
                                                        onClick={handleClick.bind(this)}
                                                        data-id={i.name}
                                                        key={i.name}>
                                                        {i.name}
                                                    </li>
                                                ):null }
                                            </ul>
                                        </Tab>
                                        <Tab eventKey="right" title={`> ${stations && stations.find(i=> i.lineId === lineStations) &&
                                        stations.find(i=> i.lineId === lineStations).points2.slice(-1)[0].name}`}>
                                            <ul className="line-stations">
                                                {
                                                    stations && stations.find(i=> i.lineId === lineStations) ?
                                                        stations.find(i=> i.lineId === lineStations).points2.map(i =>
                                                        <li className="line1-stations-item"
                                                            onClick={handleClick.bind(this)}
                                                            data-id={i.name}
                                                            key={i.name}>
                                                            {i.name}
                                                        </li>
                                                    ):null }
                                            </ul>
                                        </Tab>
                                    </Tabs>
                                </Modal.Body>
                            </Modal>
                            <Modal show={confirmSubmit} onHide={handleCloseConfirm}  centered size="sm">
                                <Modal.Body>Submit current time</Modal.Body>
                                <Modal.Footer style={{justifyContent: 'center'}}>
                                    <PersonPinIcon color="primary" className={policeSelected ? 'validation-person-clicked' : 'validation-person'} onClick={() =>{
                                        setPoliceSelected(!policeSelected)
                                    }
                                        }/>
                                    <PersonPinIcon color="action" className={conductorSelected ? 'validation-person-clicked' : 'validation-person'} onClick={() =>{
                                        setConductorSelected(!conductorSelected)
                                    }
                                        }/>
                                        <FormControl
                                            required
                                            type="text"
                                            as="textarea"
                                            name="comment"
                                            maxLength={settings && settings.commentLength}
                                            onChange={(e) => setComment(e.target.value)}
                                            placeholder="Comment..."
                                            style={{marginTop: '10px', height: '100px'}}
                                        />
                                    <Button variant="primary" onClick={addTime} style={{marginTop: '10px'}}>
                                        Submit
                                    </Button>
                                </Modal.Footer>
                            </Modal>
                            <Modal show={spamError} onHide={handleCloseConfirm}  centered size="sm">
                                <Modal.Body>You have already submitted time</Modal.Body>
                                <Modal.Footer>
                                    <Button variant="primary" onClick={() => history.push('/')}>
                                        Ok
                                    </Button>
                                </Modal.Footer>
                            </Modal>
                        </div>
                    )
            }
                <NotificationContainer/>
        </div>
    );
};

export default RegisterTicketValidation;

