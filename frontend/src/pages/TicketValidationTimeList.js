import React, {useEffect, useState} from 'react';
import { Link} from "react-router-dom";
import axios from 'axios'
import {Button, Card, OverlayTrigger, Spinner, Tooltip} from "react-bootstrap";
import moment from "moment";
import useAxios from "../hooks/useAxios";
import {NotificationContainer, NotificationManager} from "react-notifications";
import {linesInfo} from "../components/LinesInfo";
import PersonPinIcon from '@material-ui/icons/PersonPin';

const TicketValidationTimeList = () => {
    const [spinner, setSpinner] = useState(true)
    const [validationTimeList, setValidationTimeList] = useState([])
    const [stations, setStations] = useState([])
    const [title, setTitle] = useState("Default Title");
    useEffect(() => {
        let isMounted = true;
        const fetchTicketValidation = async() => {
            const getTicketValidationTime = await useAxios.get('stations/ticketValidation')

            if (isMounted && getTicketValidationTime.status === 200)
            setValidationTimeList(getTicketValidationTime.data ? getTicketValidationTime.data.sort(
                (a, b) =>  new Date(b.time).getTime() - new Date(a.time).getTime()):null)
            else if(getTicketValidationTime.error)
                NotificationManager.error('Internal Server Error', 'Error')
            setSpinner(false)

        }

        fetchTicketValidation()
        return () => { isMounted = false };

    }, []);

    useEffect(() => {
        let isMounted = true;
        const fetchSettings = async() => {
            const result = await useAxios.get('stations/settings')
            if (isMounted && result.status === 200)
                setTitle(result.data.webTitle)
            else if(result.error)
                NotificationManager.error('Internal Server Error', 'Error')
            setSpinner(false)
        }

        fetchSettings()
        return () => { isMounted = false };
    }, []);

    useEffect(() => {
        // This will run when the page first loads and whenever the title changes
        document.title = title;
    }, [title]);


    useEffect(() => {
        let isMounted = true;
        const fetchStations = async() => {
            const result = await useAxios.get('stations')
            if (isMounted)
                setStations(result.data)
        }

        fetchStations()
        return () => { isMounted = false };
    }, []);


    function diff_minutes(dt2, dt1)
    {

        var diff =(dt2.getTime() - dt1.getTime()) / 1000;
        diff /= 60;
        console.log('Math.abs(Math.round(diff))', Math.abs(Math.round(diff)))
        return Math.abs(Math.round(diff));

    }


    if (validationTimeList) {
        let arr = []

        var filteredData = validationTimeList.reduce((acc,item)=>{
            console.log('arr', arr)
            console.log('acc', acc)
            //if(arr.find(i => i.lineId === item.lineId && i.station === item.station && diff_minutes(new Date(item.time), new Date(i.time)) >= 60)){
            if(arr.find(i => i.lineId === item.lineId && i.station === item.station && diff_minutes(new Date(item.time), new Date(i.time)) >= 60)){
                acc.push(item)
                arr.push(item)

            }
    else {
                acc.push(item)
                arr.push(item)
                // acc[value1]  = (acc[value] || []).concat(item);
            }

            return acc;
        },[]);
    }

    // if (validationTimeList) {
    //     let arr = []
    //     var filteredData = validationTimeList.reduce((acc,item)=>{
    //         console.log('acc', acc)
    //         const value = item.station;
    //         const value2 = item.station + '2';
    //
    //         console.log('if ..', arr.find(i => diff_minutes(new Date(item.time), new Date(i.time)) <= 60))
    //         if(arr.find(i => i.lineId === item.lineId && i.station === item.station && diff_minutes(new Date(item.time), new Date(i.time)) <= 60)){
    //             console.log('diff', diff_minutes(new Date('2021-10-01T17:21:58+06:00'), new Date('2021-10-02T17:24:58+06:00')))
    //             console.log('item time', item.time)
    //             console.log('lineId', item.lineId)
    //             console.log('arr', arr)
    //             arr.push(item)
    //             acc[value2] = (acc[value2] || []).concat(item);
    //         }
    //           else {
    //             arr.push(item)
    //               acc[value] = (acc[value] || []).concat(item);
    //         }
    //          console.log('arr before return', arr)
    //         return acc;
    //
    //         // if(!arr.find(i => i.lineId === item.lineId && i.station === item.station && diff_minutes(new Date(item.time), new Date(i.time)) <= 60)){
    //         //     arr.push(item)
    //         //     acc.push(item)
    //         // }
    //         // return acc;
    //     },[]);
    // }
    console.log('filteredData', filteredData)
    // console.log('lsit', filteredData)

        //const arr = [ 12, 13, 14, 17, 20, 21, 27, 28, 29, 33, 34, 36, 37, 38, 41, 42, 43, 45, 46, 47, 48, 49, 50, 52, 53, 54, 56, 57, 58, 59, 60, 61, 63, 64, 65,66,69, 70, 71]

// 2, 5, 6, 3, 4, 7, 8, 9, 19, 25, 39, 44, 51, 55, 62, 81, 82, 92, 93, 97
// 72,74,75,76,77,78,79,80,83,86,87,88,89,95,98
    //57, 69, 70, 76,77,98 was empty?



    // function delay(t) {
    //     return new Promise(resolve => setTimeout(resolve, t));
    // }
    //
    // async function getResults() {
    //
    //     const results = [];
    //
    //     const strings = [69];
    //     for (let  str  of  strings) {
    //         await delay(60000);
    //         let data = await axios.get(`https://opendata-api.stib-mivb.be/NetworkDescription/1.0/PointByLine/${str}`,
    //             {headers: { Authorization: 'Bearer 2afd8233613b57938289f3ee2138290f'}  })
    //             .then(function(response) {
    //                 const responseArr = response.data.lines[0].points
    //                 const removed = responseArr.splice(19);//put
    //                 //const removed = responseArr.splice(0, 19);//post
    //
    //                 const promises = response.data.lines[0].points.map(i =>
    //                     axios.get(`https://opendata-api.stib-mivb.be/NetworkDescription/1.0/PointDetail/${i.id}`,
    //                         {headers: { Authorization: 'Bearer 2afd8233613b57938289f3ee2138290f'}  }).then(({data})=>data));
    //
    //                 return Promise.all(promises)
    //                     .then(res => {
    //                         console.log('sss', res)
    //
    //                         useAxios.put('stations',   //post
    //                             {lineId: response.data.lines[0].lineId,
    //                                 points2: response.data.lines[0].points.map(i => ({
    //                                     order: i.order,
    //                                     name: res.find(j=> j.points[0].id === i.id).points[0].name.fr
    //                                     === res.find(j=> j.points[0].id === i.id).points[0].name.nl ? res.find(j=> j.points[0].id === i.id).points[0].name.fr :
    //                                         res.find(j=> j.points[0].id === i.id).points[0].name.fr + '/' + res.find(j=> j.points[0].id === i.id).points[0].name.nl,
    //                                     gpsCoordinates: {
    //                                         latitude: res.find(j=> j.points[0].id === i.id).points[0].gpsCoordinates.latitude,
    //                                         longitude: res.find(j=> j.points[0].id === i.id).points[0].gpsCoordinates.longitude,
    //                                     }
    //                                 }))
    //                             }  )
    //                     });
    //             })
    //             .catch(err => console.log('error', err));
    //         results.push(data);
    //     }
    //     return results;
    // }

    //
    // function delay(t) {
    //     return new Promise(resolve => setTimeout(resolve, t));
    // }
    //
    // async function getResults() {
    //
    //     const results = [];
    //     const strings = [1];
    //     for (let  str  of  strings) {
    //         await delay(60000);
    //         let data = await axios.get(`https://opendata-api.stib-mivb.be/NetworkDescription/1.0/PointByLine/${str}`,
    //             {headers: { Authorization: 'Bearer 129bdf1f6b5d3c2af72d14805f0e74c9'}  })
    //             .then(function(response) {
    //                 const responseArr = response.data.lines[1].points
    //                 const removed = responseArr.splice(0, 19);//put
    //                 //const removed = responseArr.splice(19);//post
    //
    //                 const promises = response.data.lines[1].points.map(i =>
    //                     axios.get(`https://opendata-api.stib-mivb.be/NetworkDescription/1.0/PointDetail/${i.id}`,
    //                         {headers: { Authorization: 'Bearer 129bdf1f6b5d3c2af72d14805f0e74c9'}  }).then(({data})=>data));
    //
    //
    //                 return Promise.all(promises)
    //                     .then(res => {
    //                         console.log('sss', res)
    //
    //                         useAxios.put('stations',   //post
    //                             {lineId: response.data.lines[1].lineId,
    //                                 points: response.data.lines[1].points.map(i => ({order: i.order, name: res.find(j=> j.points[0].id === i.id).points[0].name.fr
    //                                     === res.find(j=> j.points[0].id === i.id).points[0].name.nl ? res.find(j=> j.points[0].id === i.id).points[0].name.fr :
    //                                         res.find(j=> j.points[0].id === i.id).points[0].name.fr + '/' + res.find(j=> j.points[0].id === i.id).points[0].name.nl
    //                                 }))
    //                             }  )
    //                     });
    //             })
    //             .catch(err => console.log('error', err));
    //         results.push(data);
    //     }
    //     return results;
    // }

    // const getData = () => {
    //
    //     axios.get('https://opendata-api.stib-mivb.be/NetworkDescription/1.0/PointByLine/97',
    //         {headers: { Authorization: 'Bearer ad8cd0288ee0d6cc1427d33990ffa308'}  })
    //         .then(function(response) {
    //              const responseArr = response.data.lines[1].points
    //             const removed = responseArr.splice(0, 19);//put
    //             //const removed = responseArr.splice(19);//post
    //
    //             const promises = response.data.lines[1].points.map(i =>
    //                 axios.get(`https://opendata-api.stib-mivb.be/NetworkDescription/1.0/PointDetail/${i.id}`,
    //                     {headers: { Authorization: 'Bearer ad8cd0288ee0d6cc1427d33990ffa308'}  }).then(({data})=>data));
    //
    //
    //             return Promise.all(promises)
    //                 .then(res => {
    //                     console.log('sss', res)
    //                     console.log('response', response)
    //                     useAxios.put('stations',   //post
    //                         {lineId: response.data.lines[1].lineId,
    //                             points: response.data.lines[1].points.map(i => ({order: i.order, name: res.find(j=> j.points[0].id === i.id).points[0].name.fr
    //                                 === res.find(j=> j.points[0].id === i.id).points[0].name.nl ? res.find(j=> j.points[0].id === i.id).points[0].name.fr :
    //                                     res.find(j=> j.points[0].id === i.id).points[0].name.fr + '/' + res.find(j=> j.points[0].id === i.id).points[0].name.nl
    //                             }))
    //                         }  )
    //                 });
    //         })
    //         .catch(err => console.log('error', err));
    // }

    // const setSettings = async() => {
    //     await useAxios.post('stations/settings')
    // }


    return (
        <div className="ticket-validation-container">
           {/*<Button onClick={() => {getResults().then(results => {*/}
           {/*    console.log(results);*/}
           {/*}).catch(err => {*/}
           {/*    console.log(err);*/}
           {/*})}}>Get</Button>*/}
           {/* <Button onClick={setSettings}>settings</Button>*/}
            <Link to='/registerTime'>
                <Button>Submit</Button>
            </Link>
            {
                spinner ? <Spinner animation="border" style={{margin: '40%', display: 'block'}}/> :
                    (
                       <div>
                           {
                               validationTimeList && filteredData.map(l => (
                                       <Card className='ticket-validation-card' style={{position: "relative"}}>
                                           <Card.Body style={{paddingTop: '5px', paddingBottom: '5px'}}>
                                               <div className="ticket-validation-line"
                                                    style={{background: linesInfo.find(i => i.line === l.lineId).background}}
                                               >
                                                   {l.lineId}
                                               </div>
                                               <span className="ticket-validation-station-name">{l.station}</span>
                                                <div style={{float: 'right', position: 'sticky'}}>
                                                    {l.person && l.person.map(i =>
                                                        <PersonPinIcon style={{ marginTop: '10px', marginRight: '10px'}} color={i === "police"? "primary": "action"}  />
                                                    )}
                                                    <OverlayTrigger
                                                        key='right'
                                                        placement='left'
                                                        overlay={
                                                            <Tooltip id='tooltip-right'>
                                                                {validationTimeList.filter(i => i.station === l.station).slice(0, 2).map(i =>
                                                                    <p>{moment(i.time).format('HH:mm')}{'   '} {i.comment}</p>
                                                                )}
                                                            </Tooltip>
                                                        }
                                                    ><div className="ticket-validation-counter">{validationTimeList.filter(i => i.station === l.station && i.lineId === l.lineId).length}</div>
                                                    </OverlayTrigger>

                                                </div>


                                               <p className="ticket-validation-time">{moment(l.time).fromNow()}</p>
                                           </Card.Body>
                                       </Card>
                                   )
                               ) }
                       </div>

                    )
            }

            <NotificationContainer/>
        </div>
    );
};

export default TicketValidationTimeList;