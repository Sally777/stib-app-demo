import React from 'react';
import {Route, Switch} from 'react-router-dom'
import TicketValidationTimeList from "./pages/TicketValidationTimeList";
import RegisterTicketValidation from "./pages/RegisterTicketValidation";


const Routes = () => {
    return (
        <Switch>
            <Route path="/"  exact component={TicketValidationTimeList}/>
            <Route path="/registerTime"  exact component={RegisterTicketValidation}/>
        </Switch>
    );
};

export default Routes;