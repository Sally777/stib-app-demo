import styled from 'styled-components';

const Container = styled.div`
    padding: 0 200px;
  margin-top: 50px;
  height: 200vh;
  
  @media (min-width: 575.98px){
    section {
      padding: 0 calc(50% - 270px);
    }
  }

  @media (min-width: 767.98px){
    section {
      padding: 0 calc(50% - 360px);
    }
  }

  @media (min-width: 991.98px) {
    section {
      padding: 0 calc(50% - 480px);
    }
  }

  @media (min-width: 1199.98px){
    section {
      padding: 0 calc(50% - 590px);
    }
  }

` ;

export default Container;