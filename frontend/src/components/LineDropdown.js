import React from 'react'
import Dropdown from "react-bootstrap/Dropdown"


const LineDropdown = ({ line, onSelect, backgroundColor, stations, validationTimeList}) => {

console.log('stations',stations )

    return (
        <div style={{marginLeft: '20px', paddingBottom: '20px'}}>
            <Dropdown onSelect={onSelect} autoClose="outside">
                            <Dropdown.Toggle style={{background: backgroundColor}} id="dropdown-basic">
                                {line}
                            </Dropdown.Toggle>
                            <Dropdown.Menu style={{background: backgroundColor}} >
                                { stations ? stations.map(i =>
                                        <Dropdown.Item eventKey={i.name} key={i.name}>
                                            {i.name}
                                        </Dropdown.Item>)
                                    :null}
                            </Dropdown.Menu>
                        </Dropdown>

        </div>
    );
};

export default LineDropdown;