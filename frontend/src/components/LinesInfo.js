export const linesInfo = [
    {
        line: '1',
        background: '#c4008f'
    },
    {
        line: '2',
        background: '#F57000'
    },
    {
        line: '5',
        background: '#E6B012'
    },
    {
        line: '6',
        background: '#0078AD'
    },
    {
        line: '3',
        background: '#b4bd10'
    },
    {
        line: '4',
        background: '#F25482'
    },
    {
        line: '7',
        background: '#ECE354'
    },
    {
        line: '8',
        background: '#0098D4'
    },
    {
        line: '9',
        background: '#CD5599'
    },
    {
        line: '19',
        background: '#DE3B21'
    },
    {
        line: '25',
        background: '#991F36'
    },
    {
        line: '39',
        background: '#DE3B21'
    },
    {
        line: '44',
        background: '#E3BA12'
    },
    {
        line: '51',
        background: '#E3BA12'
    },
    {
        line: '55',
        background: '#E3BA12'
    },
    {
        line: '62',
        background: '#FF9EC2'
    },
    {
        line: '81',
        background: '#338C26'
    },
    {
        line: '82',
        background: '#9EBFE3'
    },
    {
        line: '92',
        background: '#DE3B21'
    },
    {
        line: '93',
        background: '#E87D0D'
    },
    {
        line: '97',
        background: '#991F36'
    },
    {
        line: '12',
        background: '#c4008f'
    },
    {
        line: '13',
        background: '#F57000'
    },
    {
        line: '14',
        background: '#E6B012'
    },
    {
        line: '17',
        background: '#0078AD'
    },
    {
        line: '20',
        background: '#b4bd10'
    },
    {
        line: '21',
        background: '#F25482'
    },
    {
        line: '27',
        background: '#ECE354'
    },
    {
        line: '28',
        background: '#0098D4'
    },
    {
        line: '29',
        background: '#CD5599'
    },
    {
        line: '33',
        background: '#DE3B21'
    },
    {
        line: '34',
        background: '#991F36'
    },
    {
        line: '36',
        background: '#DE3B21'
    },
    {
        line: '37',
        background: '#E3BA12'
    },
    {
        line: '38',
        background: '#E3BA12'
    },
    {
        line: '41',
        background: '#E3BA12'
    },
    {
        line: '42',
        background: '#FF9EC2'
    },
    {
        line: '43',
        background: '#338C26'
    },
    {
        line: '45',
        background: '#9EBFE3'
    },
    {
        line: '46',
        background: '#DE3B21'
    },
    {
        line: '47',
        background: '#E87D0D'
    },
    {
        line: '48',
        background: '#991F36'
    },
    {
        line: '49',
        background: '#E3BA12'
    },
    {
        line: '50',
        background: '#E3BA12'
    },
    {
        line: '52',
        background: '#FF9EC2'
    },
    {
        line: '53',
        background: '#338C26'
    },
    {
        line: '54',
        background: '#9EBFE3'
    },
    {
        line: '56',
        background: '#DE3B21'
    },
    {
        line: '57',
        background: '#E87D0D'
    },
    {
        line: '58',
        background: '#991F36'
    },
    {
        line: '59',
        background: '#DE3B21'
    },
    {
        line: '60',
        background: '#E87D0D'
    },
    {
        line: '61',
        background: '#991F36'
    },
    {
        line: '63',
        background: '#E3BA12'
    },
    {
        line: '64',
        background: '#E3BA12'
    },
    {
        line: '65',
        background: '#FF9EC2'
    },
    {
        line: '66',
        background: '#338C26'
    },
    {
        line: '69',
        background: '#9EBFE3'
    },
    {
        line: '70',
        background: '#DE3B21'
    },
    {
        line: '71',
        background: '#E87D0D'
    },
]