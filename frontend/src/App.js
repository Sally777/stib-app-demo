import {BrowserRouter as Router} from 'react-router-dom'
import {Container} from "react-bootstrap"
import Routes from "./routes";


function App() {
  return (
      <Router>
          <div style=
                  {{background: 'linear-gradient(to bottom right, #6E009E, #e66465, #922EB6) 100%',

                  }}>
              <Container style={{padding: '20px 5% 5% 5%'}}>
                <Routes/>
              </Container>
          </div>
      </Router>

  )
}

export default App;
