const express = require('express')
const TicketValidationInfo = require('../models/TicketValidationInfo')
const Line = require('../models/Line')
const Spam = require('../models/Spam')
const Setting = require('../models/Settings')
const router = express.Router()

router.post('/', async(req, res) => {
    let line

    try {
        line = await Line.create(req.body)
    } catch (e) {
        console.log(e);
        line.error = e;
    }
    res.json(line)
})


router.put('/', async(req, res) => {
    let line

    try {
       //line = await Line.findOneAndUpdate({lineId: req.body.lineId}, {points2: req.body.points2})
        line = await Line.findOneAndUpdate({lineId: req.body.lineId}, {$push: {points2: req.body.points2}})
        console.log('line', line)
    } catch (e) {
        console.log(e);
        line.error = e;
    }
    res.json(line)
})



router.get('/', async(req, res) => {
    let response
    try {
        response = await Line.find()
    } catch (e) {
        console.log(e);
        response.error = e;
    }

    res.json(response)
})

router.post('/ticketValidation', async(req, res) => {
   let response
    try {
      response = await TicketValidationInfo.create(
          {
              time: req.body.time,
              lineId: req.body.lineId,
              station: req.body.station,
              person: req.body.person,
              comment: req.body.comment,
              ip: req.body.ip,
          })
        const spam = await Spam.create({ip: req.body.ip})
   } catch (e) {
       console.log(e);
       response.error = e;
   }

    res.json(response)
})

router.post('/settings', async(req, res) => {
    let response
    try {
        response = await Setting.create(
            {
                commentLength: '150',
                validationLength: 3600,
                spamLength: 120,
                resubmitLength: 3600,
            })
    } catch (e) {
        console.log(e);
        response.error = e;
    }

    res.json(response)
})

router.get('/settings', async(req, res) => {
    let response
    try {
        response = await Setting.findOne()
    } catch (e) {
        console.log(e);
        response.error = e;
    }

    res.json(response)
})

router.get('/ticketValidation', async(req, res) => {
    let response
    try {

        response = await TicketValidationInfo.find()
    } catch (e) {
        console.log(e);
        response.error = e;
    }

    res.json(response)
})

module.exports = router;

router.get('/spam', async(req, res) => {
    let response
    try {

        response = await Spam.find()
    } catch (e) {
        console.log(e);
        response.error = e;
    }

    res.json(response)
})

module.exports = router;