const mongoose = require('mongoose')

const Schema = mongoose.Schema
const TicketValidationInfoSchema = new Schema ({
    time: {
        type: String,
    },
    station: String,
    person: [],
    lineId: String,
    comment: String,
    ip: String,
    // expireAt: {
    //     type: Date,
    //     default: Date.now,
    // },

});
// TicketValidationInfoSchema.index({expireAt: 1}, {expireAfterSeconds: 3600})

const TicketValidationInfo = mongoose.model('TicketValidationInfo', TicketValidationInfoSchema);
module.exports = TicketValidationInfo