const mongoose = require('mongoose')

const Schema = mongoose.Schema
const SettingSchema = new Schema ({
    commentLength: String,
    validationLength: Number,
    spamLength: Number,
    resubmitLength: Number
});

const Setting = mongoose.model('Setting', SettingSchema);
module.exports = Setting