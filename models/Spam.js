const mongoose = require('mongoose')

const Schema = mongoose.Schema
const SpamSchema = new Schema ({
    ip: String,
    expireAt: {
        type: Date,
        default: Date.now,
    },
});

SpamSchema.index({expireAt: 1}, {expireAfterSeconds: 1})

const Spam = mongoose.model('Spam', SpamSchema);
module.exports = Spam