const mongoose = require('mongoose')

const Schema = mongoose.Schema
const LineSchema = new Schema ({
    lineId: String,
    points: [
                {
                    name: String,
                    order: String,
                    gpsCoordinates: {
                        latitude: Number,
                        longitude: Number,
                    }
                },
    ],
    points2: [
        {
            name: String,
            order: String,
            gpsCoordinates: {
                latitude: Number,
                longitude: Number,
            }
        },
    ],
});

const Line = mongoose.model('Line', LineSchema);
module.exports = Line